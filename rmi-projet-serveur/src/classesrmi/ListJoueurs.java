package classesrmi;

import java.io.Serializable;

public class ListJoueurs implements Serializable{

    private final int nombreJoueuers;
    private final int joueursConnecter;
    private final int id;
    private final String nom;

    public ListJoueurs(int nbrJoueurs, int joueursConnecter, int id, String nom) {
        this.nombreJoueuers = nbrJoueurs;
        this.joueursConnecter = joueursConnecter;
        this.id = id;
        this.nom = nom;
    }

    public int getNombreJoeuer() {
        return nombreJoueuers;
    }

    public int getJoueurConnecter() {
        return joueursConnecter;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }
}
