package classesrmi;

import java.io.Serializable;

public class Carte implements Serializable {

    private int couleur;
    private int numero;

    public static final int NOIR = 0;
    public static final int ROUGE = 1;
    public static final int JAUNE = 2;
    public static final int BLEUE = 3;
    public static final int VERT = 4;
    public static final int PREND_DEUX = 10;
    public static final int RENVERSE = 11;
    public static final int BLOC = 12;
    public static final int QUATRE = 13;
    public static final int JOKER = 14;

    public Carte(int couleur, int numero) {
        this.couleur = couleur;
        this.numero = numero;
    }

    public String serializCarte() {
        return couleur + " , " + numero;
    }

    @Override
    public String toString() {
        switch (numero) {
            case Carte.QUATRE:
                return "+4 (changement de couleur)";
            case Carte.JOKER:
                return "changement de couleur";
        }

        String nomCarte = "";
        boolean inversion = false;

        switch (numero) {
            case Carte.PREND_DEUX:
                nomCarte += "+2 ";
                break;
            case Carte.BLOC:
                nomCarte += "Bloc ";
                break;
            case Carte.RENVERSE:
                nomCarte += "Renverse ";
                inversion = true;
                break;
            default:
                nomCarte += numero + " ";
                break;
        }

        switch (couleur) {
            case Carte.JAUNE:
                if (inversion) {
                    nomCarte += "jaune";
                } else {
                    nomCarte += "jaune";
                }
                break;
            case Carte.ROUGE:
                if (inversion) {
                    nomCarte += "rouge";
                } else {
                    nomCarte += "rouge";
                }
                break;
            case Carte.VERT:
                nomCarte += "vert";
                break;
            case Carte.BLEUE:
                nomCarte += "bleue";
                break;
        }
        return nomCarte;
    }

    
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Carte other = (Carte) obj;
        if (this.couleur != other.couleur) {
            return false;
        }
        return this.numero == other.numero;
    }

    public int getCouleur() {
        return couleur;
    }

    public void setColeur(int couleur) {
        this.couleur = couleur;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
}
