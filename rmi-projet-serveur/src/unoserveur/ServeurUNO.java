package unoserveur;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;



public class ServeurUNO  {

    public static final int PORT = 12345;

    public static void main(String[] args) {
    	new Thread(() -> initialiseServeur()).start();
    }
    private static void initialiseServeur() {
    	try {
            LocateRegistry.createRegistry(PORT);
            Registry register = LocateRegistry.getRegistry(PORT);
            register.rebind("Gestionnaire de partie", GestionnairePartie.getgestionnaire());
            System.out.println("Serveur est lanc� !!");
        } catch (RemoteException ex) {
            System.out.println(ex.toString());
        }
    }
}
