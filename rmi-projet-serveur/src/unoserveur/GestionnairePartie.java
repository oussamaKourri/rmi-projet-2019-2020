package unoserveur;

import static unoserveur.ServeurUNO.PORT;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import classesrmi.*;
import interfacesrmi.*;


public class GestionnairePartie extends UnicastRemoteObject implements IGestionnairePartie, Serializable{

    private final Map<Integer, Partie> partie;
    private final Registry register;
	private static int ID = 0;
    private static int ID_JOUEUR = 0;

    private static GestionnairePartie gestionnaire = null;

    
    private GestionnairePartie() throws RemoteException{
        register = LocateRegistry.getRegistry(PORT);
        partie = new ConcurrentHashMap<>();
    }
    
    
    public IPartie entrerPartie(int idPartie, int idjoueur) throws RemoteException {
        try {
            Partie p = partie.get(idPartie);
            
            IComunicateurJoueur joueur = (IComunicateurJoueur) register.lookup(Serviable.getnomjoueur(idjoueur));
            
            if (p.getJoueursConnectes() == p.getNbreJoueurs()) {
                joueur.partieComplete();
                return null;
            }
            
            p.addJoueur(joueur);
            
            if (p.getJoueursConnectes() == p.getNbreJoueurs())
                p.joueursConnectes();
            
            return (IPartie) register.lookup(Serviable.getNomPartie(idPartie));
        } catch (RemoteException | NotBoundException ex) {
            return null;
        }
    }
    
    
    public IPartie creePartie(String nom, int njoueures, int idPremierJoueur) throws RemoteException {
        try {
        	IComunicateurJoueur comunicateurPremierJoueur = (IComunicateurJoueur) register.lookup(Serviable.getnomjoueur(idPremierJoueur));
            
            int id = ID++;
            Partie p = new Partie(njoueures, id, nom, comunicateurPremierJoueur);
            partie.put(id, p);
            register.bind(Serviable.getNomPartie(id), p);
            return (IPartie) register.lookup(Serviable.getNomPartie(id));
        } catch (NotBoundException | AccessException | AlreadyBoundException ex) {
            return null;
        }
    }

    
    public int joueurConnecte() throws RemoteException{
        return ID_JOUEUR++;
    }
    
    
    public List<ListJoueurs> listePartie() throws RemoteException {
        List<ListJoueurs> listePartie = new ArrayList<>();

		partie.entrySet().stream().map((e) -> e.getValue()).forEach((partie) -> {
            listePartie.add(new ListJoueurs(partie.getNbreJoueurs(), partie.getJoueursConnectes(), partie.getId(), partie.getnom()));
        });
        
        return listePartie;
    }

    
    public static GestionnairePartie getgestionnaire() {
        if (gestionnaire == null) {
            try {
                gestionnaire = new GestionnairePartie();
            } catch (RemoteException ex) {
            }
        }

        return gestionnaire;
    }
}
