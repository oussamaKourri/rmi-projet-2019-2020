package unoserveur;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import classesrmi.Carte;
import interfacesrmi.*;


public class Partie extends UnicastRemoteObject implements IPartie, Serializable {

    private CarteJeu paquet;
    private final Map<Integer, IComunicateurJoueur> joueurs;

    private final int nbreJoueurs;
    private int joueursEnLigne;
    private final int id;
    private final String nom;

    private int joueurActuel = 1;
	private boolean sensHoraire = true;
	private boolean encoursdeprendre = false;
    private int nombreActuel = 0;

    public Partie(int nbreJoueurs, int id, String nom, IComunicateurJoueur premierJoueur) throws RemoteException {
        this.nbreJoueurs = nbreJoueurs;
        this.id = id;
        this.nom = nom;
        this.joueurs = new HashMap<>();
        this.joueursEnLigne = 0;
        this.paquet = null;
        addJoueur(premierJoueur);
    }

    
    public void passerJeu() throws RemoteException {
        incrementerTourJoueur(1, sensHoraire);
        raportJoueur(false, true);
    }

    
    public void prendreCarte() throws RemoteException {
        List<Carte> listCarte = new ArrayList<>();

        if (encoursdeprendre) {
            for (int i = 0; i < nombreActuel; i++) {
                listCarte.add(this.paquet.getCarteTop());
            }
            encoursdeprendre = false;
            nombreActuel = 0;
        } else {
            listCarte.add(this.paquet.getCarteTop());
        }
        
        joueurs.get(joueurActuel).retourAjoutCarte(listCarte);
        raportJoueur(false, false);
    }
    
    
	public void jouerCarte(Carte CarteJouer) throws RemoteException {
        this.paquet.jouerCarte(CarteJouer);

        int numero = CarteJouer.getNumero();

        switch (numero) {
            case Carte.PREND_DEUX:
                nombreActuel += 2;
                encoursdeprendre = true;
                incrementerTourJoueur(1, sensHoraire);
                break;
            case Carte.QUATRE:
                nombreActuel += 4;
                encoursdeprendre = true;
                incrementerTourJoueur(1, sensHoraire);
                break;
            case Carte.BLOC:
                incrementerTourJoueur(2, sensHoraire);
                break;
            case Carte.RENVERSE:
            	sensHoraire = !sensHoraire;
                incrementerTourJoueur(1, sensHoraire);
                break;
            default:
                incrementerTourJoueur(1, sensHoraire);
                break;
        }

        raportJoueur(true, true);
    }

	private void raportJoueur(boolean changerCarteTable, boolean changeProchainJoueur) {
        joueurs.entrySet().stream().map((e) -> e.getValue()).forEach((joueur) -> {
            try {
                if (changerCarteTable) {
                    joueur.setCarteTable(this.paquet.getCarteTable());
                }

                if (changeProchainJoueur) {
                    joueur.setJoueurSuivant(this.joueurActuel);
                }

                joueur.setScore(this.encoursdeprendre, this.nombreActuel);
            } catch (RemoteException ex) {
                System.out.println(ex.toString());
            }
        });
    }


    public void declarerVictoir(int numeroJoueur) throws RemoteException {
        joueurs.entrySet().stream().map((e) -> e.getValue()).forEach((joueur) -> {
            try {
                joueur.joueurGagne(numeroJoueur);
            } catch (RemoteException ex) {
            	System.out.println(ex.toString());
            }
        });
    }

    
    public void declarerSortieJeu(int numeroJoueur) throws RemoteException {
        joueurs.entrySet().stream().map((e) -> e.getValue()).forEach((joueur) -> {
            try {
                joueur.quitteEtatUno(numeroJoueur);
            } catch (RemoteException ex) {
            	System.out.println(ex.toString());
            }
        });
    }

    @Override
    public void declarerUno(int numeroJoueur) throws RemoteException {
        joueurs.entrySet().stream().map((e) -> e.getValue()).forEach((joueur) -> {
            try {
                joueur.declarerUno(numeroJoueur);
            } catch (RemoteException ex) {
            	System.out.println(ex.toString());
            }
        });
    }

    private void setPremierCarteTable() {
        paquet.jouerCarte(paquet.getCarteTop());

        Carte carte = paquet.getCarteTable();

        joueurs.entrySet().stream().map((e) -> e.getValue()).forEach((joueur) -> {
            try {
                joueur.setCartePremiere(carte);
            } catch (RemoteException ex) {
            	System.out.println(ex.toString());
            }
        });
    }

    private void distribuerNumeroJoueures() {
        joueurs.entrySet().stream().forEach((joueur) -> {
            try {
                Integer numeroJoueur = joueur.getKey();
                IComunicateurJoueur com = joueur.getValue();
                com.setNumJoueur(numeroJoueur);
            } catch (RemoteException ex) {
            	System.out.println(ex.toString());
            }
        });
    }

    private void distribuerCartes() {
        this.paquet = new CarteJeu();

        joueurs.entrySet().stream().map((e) -> e.getValue()).forEach((joueur) -> {
            try {
                List<Carte> liste = new ArrayList<>();

                for (int j = 0; j < 7; j++) {
                    Carte c = paquet.getCarteTop();
                    liste.add(c);
                }

                joueur.setCarteMain(liste);
            } catch (RemoteException ex) {
            	System.out.println(ex.toString());
            }
        });
    }

    private void incrementerTourJoueur(int valeur, boolean sensHoraire) {
        if (sensHoraire) {
            for (int i = 1; i <= valeur; i++) {
                if (joueurActuel == nbreJoueurs) {
                    joueurActuel = 1;
                } else {
                    joueurActuel++;
                }
            }
        } else {
            for (int i = 1; i <= valeur; i++) {
                if (joueurActuel == 1) {
                    joueurActuel = nbreJoueurs;
                } else {
                    joueurActuel--;
                }
            }
        }
    }

    public void joueursConnectes() {
        joueurs.entrySet().stream().map((e) -> e.getValue()).forEach((joueur) -> {
            try {
                joueur.setJoueursConnectes(true);
            } catch (RemoteException ex) {
            	System.out.println(ex.toString());
            }
        });

        initialiserPartie();
    }
    
    public void initialiserPartie() {
        distribuerCartes();
        distribuerNumeroJoueures();
        setPremierCarteTable();
    }

    public final void addJoueur(IComunicateurJoueur joueur) {
        this.joueurs.put(++joueursEnLigne, joueur);
    }

    public int getId() {
        return id;
    }

    public int getNbreJoueurs() {
        return nbreJoueurs;
    }

    public int getJoueursConnectes() {
        return joueursEnLigne;
    }

    public String getnom() {
        return nom;
    }

}
