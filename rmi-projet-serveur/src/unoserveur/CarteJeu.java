package unoserveur;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import classesrmi.Carte;

public class CarteJeu {

    private Carte CarteTable;
    private List<Carte> CarteJoueurs;
    private List<Carte> Cartes;
   
    public CarteJeu() {
        this.Cartes = new ArrayList<>();
        initialiseNormale() ;
        this.CarteJoueurs = new ArrayList<>();
    }

    private void initialiseNormale() {
        for (int i = 0; i <= 12; i++) {
            if (i == 0) {
                this.Cartes.add(new Carte(Carte.ROUGE, 0));
                this.Cartes.add(new Carte(Carte.BLEUE, 0));
                this.Cartes.add(new Carte(Carte.JAUNE, 0));
                this.Cartes.add(new Carte(Carte.VERT, 0));
            } else {
                this.Cartes.add(new Carte(Carte.ROUGE, i));
                this.Cartes.add(new Carte(Carte.ROUGE, i));
                this.Cartes.add(new Carte(Carte.BLEUE, i));
                this.Cartes.add(new Carte(Carte.BLEUE, i));
                this.Cartes.add(new Carte(Carte.JAUNE, i));
                this.Cartes.add(new Carte(Carte.JAUNE, i));
                this.Cartes.add(new Carte(Carte.VERT, i));
                this.Cartes.add(new Carte(Carte.VERT, i));
            }
        }

        this.Cartes.add(new Carte(Carte.NOIR, Carte.JOKER));
        this.Cartes.add(new Carte(Carte.NOIR, Carte.JOKER));
        this.Cartes.add(new Carte(Carte.NOIR, Carte.JOKER));
        this.Cartes.add(new Carte(Carte.NOIR, Carte.JOKER));
        this.Cartes.add(new Carte(Carte.NOIR, Carte.QUATRE));
        this.Cartes.add(new Carte(Carte.NOIR, Carte.QUATRE));
        this.Cartes.add(new Carte(Carte.NOIR, Carte.QUATRE));
        this.Cartes.add(new Carte(Carte.NOIR, Carte.QUATRE));

        this.Cartes.add(new Carte(Carte.ROUGE, Carte.BLOC));
        this.Cartes.add(new Carte(Carte.VERT, Carte.BLOC));
        this.Cartes.add(new Carte(Carte.JAUNE, Carte.BLOC));
        this.Cartes.add(new Carte(Carte.BLEUE, Carte.BLOC));
        
        this.Cartes.add(new Carte(Carte.ROUGE, Carte.RENVERSE));
        this.Cartes.add(new Carte(Carte.VERT, Carte.RENVERSE));
        this.Cartes.add(new Carte(Carte.JAUNE, Carte.RENVERSE));
        this.Cartes.add(new Carte(Carte.BLEUE, Carte.RENVERSE));
        
        Collections.shuffle(Cartes);
    }

    public void jouerCarte(Carte c) {
    	CarteJoueurs.add(CarteTable);
        CarteTable = c;
    }

    public Carte getCarteTop() {
        if (Cartes.isEmpty()) {
            Cartes = new ArrayList<>(CarteJoueurs);
            Collections.shuffle(Cartes);
        }

        Carte c = Cartes.remove(Cartes.size() - 1);
        return c;
    }

    public Carte getCarteTable() {
        return CarteTable;
    }

    public void setCarteTable(Carte CarteTable) {
        this.CarteTable = CarteTable;
    }

    public List<Carte> getCartesJoeurs() {
        return CarteJoueurs;
    }

    public void setCartesJoeurs(List<Carte> CartesJoeurs) {
		this.CarteJoueurs = CartesJoeurs;
    }

    public List<Carte> getCartes() {
        return Cartes;
    }

    public void setCartes(List<Carte> Cartes) {
        this.Cartes = Cartes;
    }
}
