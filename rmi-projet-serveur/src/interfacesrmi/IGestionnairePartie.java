package interfacesrmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import classesrmi.ListJoueurs;

public interface IGestionnairePartie extends Remote {
	public IPartie creePartie(String nom, int nbreJoueurs, int idPremierJoueur) throws RemoteException;
    public int joueurConnecte() throws RemoteException;
    public List<ListJoueurs> listePartie() throws RemoteException;
    public IPartie entrerPartie(int id, int joueur) throws RemoteException;
}
