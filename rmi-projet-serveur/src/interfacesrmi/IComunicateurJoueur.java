package interfacesrmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import classesrmi.Carte;

public interface IComunicateurJoueur extends Remote {
	public void setJoueurSuivant(int joueur) throws RemoteException;
    public void setCarteTable(Carte c) throws RemoteException;
    public void declarerUno(int joueur) throws RemoteException;
    public void quitteEtatUno(int joueur) throws RemoteException;
    public void joueurGagne(int joueur) throws RemoteException;
    public void setScore(boolean actuel, int score) throws RemoteException;
    public void retourAjoutCarte(List<Carte> carte) throws RemoteException;
	public void setJoueursConnectes(boolean tousconnectes) throws RemoteException;
	public void partieComplete() throws RemoteException;
	public void setCarteMain(List<Carte> mainJoueur) throws RemoteException;
	public void setNumJoueur(int numeroJoueur) throws RemoteException;
	public int getId() throws RemoteException;
	public void setCartePremiere(Carte c) throws RemoteException;
}
