package interfacesrmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import classesrmi.Carte;

public interface IPartie extends Remote {   
    
    public void jouerCarte(Carte c) throws RemoteException;
    public void prendreCarte() throws RemoteException;
    public void declarerVictoir(int numeroJoueur) throws RemoteException;
    public void declarerSortieJeu(int numeroJoueur) throws RemoteException;
    public void declarerUno(int numeroJoueur) throws RemoteException;
    public void passerJeu() throws RemoteException;
    
}
