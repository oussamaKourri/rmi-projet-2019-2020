package unoclient;

import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import classesrmi.ListJoueurs;
import interfacesrmi.IGestionnairePartie;
import interfacesrmi.IPartie;
import java.rmi.RemoteException;

public class EcranListePatie {
    
    private IGestionnairePartie gestionnaire;
    private TableView<ListJoueurs> table;
    
    public void initialiseEcran(IGestionnairePartie gestionnaire, List<ListJoueurs> parties, Communicateur communicateur) {
        this.gestionnaire = gestionnaire;
        
        ObservableList<ListJoueurs> listePartie = FXCollections.observableArrayList();
        
        parties.stream().forEach((partie) -> {
            listePartie.add(partie);
        });
        
        BorderPane root;
        
        Label label = new Label("parties disponible");
        label.setFont(new Font("Arial", 20));
        label.setAlignment(Pos.CENTER);
        
        table = new TableView<>();
        table.setEditable(false);
        
        TableColumn nomColonne = new TableColumn("nom de la partie");
        nomColonne.setCellValueFactory(new PropertyValueFactory<>("nom"));
 
        table.setItems(listePartie);
        table.getColumns().addAll(nomColonne);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
        VBox vbox = new VBox();
        vbox.setSpacing(20);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(label, table);
        vbox.setAlignment(Pos.CENTER);
        
        HBox hbox = new HBox(vbox);
        hbox.setAlignment(Pos.CENTER);
        
        root = new BorderPane(hbox);
        
        Button buttonRetour = new Button("Retour");
        buttonRetour.setOnAction((ActionEvent event) -> {
            new EcranAccueil().initialiseEcran(gestionnaire, communicateur);
        });
        
        Button buttonEntrer = new Button("Entrer");
        buttonEntrer.setOnAction((ActionEvent event) -> {
            ListJoueurs partieSelectionne = table.getSelectionModel().getSelectedItem();
            
            if (partieSelectionne != null) {
                try {
                    IPartie partie = this.gestionnaire.entrerPartie(partieSelectionne.getId(), communicateur.getId());
                    
                    if (partie != null)
                        new EcranAttenteJoueur().EcranDemarage(gestionnaire, communicateur, partie);
                } catch (RemoteException ex) {
                    System.out.println(ex.toString());
                }
            }
        });
        
        HBox hbox2 = new HBox(buttonRetour, buttonEntrer);
        hbox2.setSpacing(10);
        hbox2.setPadding(new Insets(10));
        
        root.setBottom(hbox2);
        
        ClientRMI.setScene(root);
    }
}
