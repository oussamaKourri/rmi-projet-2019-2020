package unoclient;

import interfacesrmi.IPartie;
import java.util.List;
import java.util.Optional;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import classesrmi.Carte;

import java.rmi.RemoteException;

public class EcranPartie {

    private int numeroJoueur;
    private int tourJoueur;
    private boolean prendreCarte;
    private int quantiteCartePrie;
    private boolean uno;
    private boolean dejaPrie;
    private boolean finJeu;

    private Carte carteSurTable;

    private List<Carte> mainJoueur;

    private Communicateur communicateur;
    private IPartie partie;

    private StackPane carteSurTablePane;

    private int couleurSelectionne;

    private Label tourJoueurLabel;
    private Label joueurLabel;
    private Button jouer;
    private Button prendre;
    private ListView<Carte> listeCarte;
    private StackPane carteApercu;

    private static EcranPartie ecranPartie;

    private EcranPartie() {
        listeCarte = new ListView<>();
        mainJoueur = null;
    }

    public static EcranPartie getEcranPartie() {
        if (ecranPartie == null) {
            ecranPartie = new EcranPartie();
        }

        return ecranPartie;
    }

    public static void deleteecranPartie() {
        ecranPartie = null;
    }

    public void initialiseEcran(Communicateur communicateur, IPartie partie) {
        this.communicateur = communicateur;
        this.partie = partie;
        carteSurTable = null;
        couleurSelectionne = -1;
        jouer = new Button("Jouer");
        prendre = new Button("Prendre");
        tourJoueurLabel = new Label();
        joueurLabel = new Label();
        carteApercu = new StackPane();
        numeroJoueur = -1;
        tourJoueur = 1;
        prendreCarte = false;
        quantiteCartePrie = 0;
        uno = false;
        dejaPrie = false;
        finJeu = false;
        listeCarte = new ListView<>();
        mainJoueur = null;

        BorderPane root;
        BorderPane carteTable;

        carteSurTablePane = new StackPane();

        carteTable = new BorderPane(carteSurTablePane);

        root = new BorderPane(carteTable);

        Button quitter = new Button("Quitter la partie");
        	quitter.setOnAction(value -> {
        });
        HBox hboxQuitter = new HBox(quitter);
        hboxQuitter.setPadding(new Insets(10));

        HBox hBox = new HBox();
        hBox.setSpacing(10);

        jouer.setDisable(true);
        jouer.setOnAction(value -> {
            if (!finJeu) {
                if (numeroJoueur == tourJoueur) {
                    Carte carteJouer = listeCarte.getSelectionModel().getSelectedItem();

                    if (jeuValide(carteJouer)) {
                        if (dejaPrie) {
                            dejaPrie = false;
                            prendre.setText("prendre");
                        }
                        if (carteJouer.getCouleur() == Carte.NOIR) {
                            Dialog<Integer> dialog = new Dialog<>();
                            dialog.setResizable(true);

                            int taille = 100;
                            // couleur avant que je selectionne
                            Color initialeRouge = Color.rgb(96, 0, 0);
                            Color initialeBleu = Color.rgb(0, 0, 96);
                            Color initialeJaune = Color.rgb(215, 215, 0);
                            Color initialeVert = Color.rgb(0, 96, 0);
                            // couleur apr�s que je selectionne
                            Color rougeSelectionne = Color.RED;
                            Color bleueSelectionne = Color.BLUE;
                            Color jauneSelectionne = Color.YELLOW;
                            Color vertSelectionne = Color.GREEN;

                            Rectangle rouge = new Rectangle(taille, taille, initialeRouge);
                            Rectangle bleu = new Rectangle(taille, taille, initialeBleu);
                            Rectangle jaune = new Rectangle(taille, taille, initialeJaune);
                            Rectangle vert = new Rectangle(taille, taille, initialeVert);

                            GridPane grid = new GridPane();
                            grid.add(rouge, 2, 1);
                            grid.add(bleu, 1, 1);
                            grid.add(jaune, 2, 2);
                            grid.add(vert, 1, 2);
                            dialog.getDialogPane().setContent(grid);

                            ButtonType buttonValide = new ButtonType("Valider le choix", ButtonBar.ButtonData.OK_DONE);
                            dialog.getDialogPane().getButtonTypes().add(buttonValide);

                            final Button okButton = (Button) dialog.getDialogPane().lookupButton(buttonValide);
                            okButton.setDisable(true);

                            rouge.setOnMouseClicked(event -> {
                                rouge.setFill(rougeSelectionne);
                                bleu.setFill(initialeBleu);
                                vert.setFill(initialeVert);
                                jaune.setFill(initialeJaune);
                                couleurSelectionne = Carte.ROUGE;
                                okButton.setDisable(false);
                            });

                            bleu.setOnMouseClicked(event -> {
                                bleu.setFill(bleueSelectionne);
                                rouge.setFill(initialeRouge);
                                vert.setFill(initialeVert);
                                jaune.setFill(initialeJaune);
                                couleurSelectionne = Carte.BLEUE;
                                okButton.setDisable(false);
                            });

                            jaune.setOnMouseClicked(event -> {
                                jaune.setFill(jauneSelectionne);
                                rouge.setFill(initialeRouge);
                                bleu.setFill(initialeBleu);
                                vert.setFill(initialeVert);
                                couleurSelectionne = Carte.JAUNE;
                                okButton.setDisable(false);
                            });

                            vert.setOnMouseClicked(event -> {
                                vert.setFill(vertSelectionne);
                                rouge.setFill(initialeRouge);
                                bleu.setFill(initialeBleu);
                                jaune.setFill(initialeJaune);
                                couleurSelectionne = Carte.VERT;
                                okButton.setDisable(false);
                            });

                            dialog.setResultConverter((ButtonType b) -> {
                                if (b == buttonValide) {
                                    return couleurSelectionne;
                                }

                                couleurSelectionne = -1;
                                return null;
                            });

                            Optional<Integer> result = dialog.showAndWait();

                            if (result.isPresent()) {
                                Integer novaCor = result.get();
                                carteJouer.setColeur(novaCor);

                                joueurCarte(carteJouer);
                            }
                        } else {
                            joueurCarte(carteJouer);
                        }
                    } else {
                        ClientRMI.messageErreur("Jeu invalide");
                    }
                } else {
                    ClientRMI.messageErreur("Attends ton tour");
                }
            } else {
                ClientRMI.messageErreur("La partie est termin�");
            }
        });

        prendre.setOnAction(value -> {
            if (!finJeu) {
                if (numeroJoueur == tourJoueur) {
                    if (!dejaPrie) {
                        prendre.setText("Passer");
                        dejaPrie = true;

                        tirerCarte();
                    } else {
                        try {
                            prendre.setText("prendre");
                            dejaPrie = false;
                            this.partie.passerJeu();;
                        } catch (RemoteException ex) {
                            
                        }
                    }
                    
                    listeCarte.getSelectionModel().clearSelection();
                } else {
                    ClientRMI.messageErreur("Attends ton tour");
                }
            } else {
                ClientRMI.messageErreur("La partie est termin�");
            }
        });

        hBox.getChildren().addAll(jouer, prendre);

        VBox vBox = new VBox(listeCarte, carteApercu, hBox);
        vBox.setPadding(new Insets(10));
        vBox.setSpacing(30);

        setTimeJoueur(null);

        VBox vboxTop = new VBox(joueurLabel, tourJoueurLabel);
        vboxTop.setPadding(new Insets(10));
        vboxTop.setAlignment(Pos.CENTER);

        root.setRight(vboxTop);
        root.setLeft(vBox);
        root.setBottom(hboxQuitter);

        ClientRMI.setScene(root);

        new Thread(() -> initJeu()).start();
    }

    private void tirerCarte() {
        try {
            if (uno) {
                uno = false;
                this.partie.declarerSortieJeu(numeroJoueur);
            }

            this.partie.prendreCarte();;
        } catch (RemoteException ex) {
            
        }
    }

    private void joueurCarte(Carte carteJouer) {
        try {
            if (listeCarte.getItems().size() == 2 && !uno) {
                this.partie.declarerUno(numeroJoueur);
                uno = true;
            }

            if (listeCarte.getItems().size() == 1 && uno) {
                this.partie.declarerVictoir(numeroJoueur);
            }

            this.partie.jouerCarte(carteJouer);
            listeCarte.getItems().remove(listeCarte.getSelectionModel().getSelectedIndex());
            setTimeJoueur(null);
            listeCarte.getSelectionModel().clearSelection();
        } catch (RemoteException ex) {
           
        }
    }

    private boolean jeuValide(Carte carteChoisie) {
        int couleurCarteChoisie = carteChoisie.getCouleur();
        int numeroCarteChoisie = carteChoisie.getNumero();
        int couleurCarteTable = carteSurTable.getCouleur();
        int numeroCarteSurTable = carteSurTable.getNumero();

        if ((numeroCarteSurTable == Carte.PREND_DEUX || numeroCarteSurTable == Carte.QUATRE) && prendreCarte) {
            return numeroCarteChoisie == Carte.PREND_DEUX || numeroCarteChoisie == Carte.QUATRE;
        }

        if (numeroCarteChoisie == Carte.QUATRE || numeroCarteChoisie == Carte.JOKER) {
            return true;
        }

        return couleurCarteChoisie == couleurCarteTable || numeroCarteChoisie == numeroCarteSurTable;
    }

    private void initJeu() {
        try {
            while (communicateur.getListCartes() == null) {
                Thread.yield();
            }

            setMainJoueur(communicateur.getListCartes());
            communicateur.setCarteMain(null);

            while (communicateur.getNumeroJoueur() == 0) {
                Thread.yield();
            }

            setNumJoueur(communicateur.getNumeroJoueur());

            while (communicateur.getCarteTable() == null) {
                Thread.yield();
            }

			setCarteTable(communicateur.getCarteTable());
            communicateur.setCartePremiere(null);
        } catch (RemoteException ex) {
           
        }
    }

    public void ajouterCarte(List<Carte> carte) {
        Platform.runLater(() -> {
            carte.stream().forEach((c) -> {
                mainJoueur.add(c);
                listeCarte.getItems().add(c);
            });
        });
    }

    public void setCartePrise(boolean actuel) {
        prendreCarte = actuel;
    }

    public void setNumJoueur(int numero) {
        numeroJoueur = numero;

        Platform.runLater(() -> {
            joueurLabel.setText("Vous �tes le joueur " + numeroJoueur);

            if (numeroJoueur == 1) {
                tourJoueurLabel.setText("Votre tour");
                tourJoueurLabel.setTextFill(Color.GREEN);
            } else {
                tourJoueurLabel.setText("Tour du joueur 1");
                tourJoueurLabel.setTextFill(Color.RED);
            }
        });
    }

    public void setNbreCartePrise(int score) {
        if (score != quantiteCartePrie) {
            quantiteCartePrie = score;
        }
    }

    public void setTourJoueur(int joueur) {
        tourJoueur = joueur;

        Platform.runLater(() -> {
            if (tourJoueur == numeroJoueur) {
                tourJoueurLabel.setText("Votre tour");
                tourJoueurLabel.setTextFill(Color.GREEN);
            } else {
                tourJoueurLabel.setText("Tour du joueur " + tourJoueur);
                tourJoueurLabel.setTextFill(Color.RED);
            }
            listeCarte.getSelectionModel().clearSelection();
        });
    }

    private void setTimeJoueur(Carte c) {
        Platform.runLater(() -> {
            carteApercu.getChildren().clear();

            if (c == null) {
                carteApercu.getChildren().addAll(CarteVisu.genererVisuCarte(-1, 0));
            } else {
                carteApercu.getChildren().addAll(CarteVisu.genererVisuCarte(c.getCouleur(), c.getNumero()));
            }
        });
    }

    public void setCarteTable(Carte c) {
        carteSurTable = c;
        Platform.runLater(() -> {
            carteSurTablePane.getChildren().clear();
            carteSurTablePane.getChildren().addAll(CarteVisu.genererVisuCarte(c.getCouleur(), c.getNumero()));
        });
    }

    private void setMainJoueur(List<Carte> mainJoueur) {
        this.mainJoueur = mainJoueur;

        ObservableList<Carte> carte = FXCollections.observableArrayList();

        for (int i = 0; i < mainJoueur.size(); i++) {
            carte.add(mainJoueur.get(i));
        }

        listeCarte.setItems(carte);
        listeCarte.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Carte> observable, Carte oldValue, Carte newValue) -> {
            jouer.setDisable(numeroJoueur != tourJoueur);
            setTimeJoueur(newValue);
        });
    }

    public void finPartie() {
        this.finJeu = true;
    }
}
