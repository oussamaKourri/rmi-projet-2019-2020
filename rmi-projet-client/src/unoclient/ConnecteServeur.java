package unoclient;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import interfacesrmi.IGestionnairePartie;
import java.rmi.AlreadyBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ConnecteServeur {
 
    IGestionnairePartie gestionnaire;
    
    public void initialiseEcran(String ip) {
        Text text = new Text("Connexion au serveur");
        text.setFont(Font.font("Arial", FontWeight.NORMAL, 20));

        
        VBox vBox = new VBox(text);
        vBox.setSpacing(20);
        vBox.setAlignment(Pos.CENTER);
        BorderPane borderPane = new BorderPane(vBox);

        Button retour = new Button("Retour");
        retour.setOnAction(event -> {
            ClientRMI.creeScene();
        });

        HBox hBox = new HBox(retour);
        hBox.setPadding(new Insets(15));

        borderPane.setBottom(hBox);

        ClientRMI.setScene(borderPane);
        
        new Thread(() -> connecteServeur(ip)).start();
    }
    
    private void connecteServeur(String ip) {
        try {
			Registry r = LocateRegistry.getRegistry(ip, ClientRMI.PORT);
            gestionnaire = (IGestionnairePartie) r.lookup("Gestionnaire de partie");
            
            int idjoueur = gestionnaire.joueurConnecte();
            
            Communicateur c = new Communicateur(idjoueur);
            r.bind("Comunicateur_" + idjoueur, c);
            
            new EcranAccueil().initialiseEcran(gestionnaire, c);
        } catch (NotBoundException | RemoteException | AlreadyBoundException ex) {
            System.out.println(ex.toString());
        }
    }
}
