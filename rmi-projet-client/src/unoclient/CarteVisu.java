package unoclient;

import java.util.ArrayDeque;
import java.util.Deque;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import classesrmi.Carte;

public class CarteVisu {

    public static Node genererVisuCarte(int couleur, int numero) {
            
            Deque<Node> children = new ArrayDeque<>();

            if (isSpecial(numero)) {
                switch (numero) {
                    case Carte.PREND_DEUX: {
                        Text plusDeux = new Text("+2");
                        int sizeTexte = (int) Math.rint(100);
                        plusDeux.setFont(new Font(sizeTexte));
                        switch (couleur) {
	                        case Carte.NOIR:
	                        	plusDeux.setFill(Color.BLACK);
	                            break;
	                        case Carte.ROUGE:
	                        	plusDeux.setFill(Color.RED);
	                            break;
	                        case Carte.JAUNE:
	                        	plusDeux.setFill(Color.rgb(255, 190, 0));
	                            break;
	                        case Carte.BLEUE:
	                        	plusDeux.setFill(Color.BLUE);
	                            break;
	                        case Carte.VERT:
	                        	plusDeux.setFill(Color.GREEN);
	                            break;
                        }
                        children.add(plusDeux);
                        break;
                    }
                    case Carte.QUATRE: {
                    	Text plusQuatre = new Text("+ 4 ");
                    	int sizeTexte = (int) Math.rint(100);
                    	plusQuatre.setFont(new Font(sizeTexte));
                        switch (couleur) {
	                        case Carte.NOIR:
	                        	plusQuatre.setFill(Color.BLACK);
	                            break;
	                        case Carte.ROUGE:
	                        	plusQuatre.setFill(Color.RED);
	                            break;
	                        case Carte.JAUNE:
	                        	plusQuatre.setFill(Color.YELLOW);
	                            break;
	                        case Carte.BLEUE:
	                        	plusQuatre.setFill(Color.BLUE);
	                            break;
	                        case Carte.VERT:
	                        	plusQuatre.setFill(Color.GREEN);
	                            break;
                        }
                        children.add(plusQuatre);

                        break;
                    }
                    case Carte.RENVERSE:{
                    	Text renverse = new Text("Renverser le Jeu");
                    	int sizeTexte = (int) Math.rint(20);
                    	renverse.setFont(new Font(sizeTexte));
                        switch (couleur) {
	                        case Carte.NOIR:
	                        	renverse.setFill(Color.BLACK);
	                            break;
	                        case Carte.ROUGE:
	                        	renverse.setFill(Color.RED);
	                            break;
	                        case Carte.JAUNE:
	                        	renverse.setFill(Color.YELLOW);
	                            break;
	                        case Carte.BLEUE:
	                        	renverse.setFill(Color.BLUE);
	                            break;
	                        case Carte.VERT:
	                        	renverse.setFill(Color.GREEN);
	                            break;
                        }
                        children.add(renverse);

                        break;
                    }
                    case Carte.BLOC: {
                    	Text bloc = new Text("Bloc");
                    	int sizeTexte = (int) Math.rint(40);
                    	bloc.setFont(new Font(sizeTexte));
                        switch (couleur) {
	                        case Carte.NOIR:
	                        	bloc.setFill(Color.BLACK);
	                            break;
	                        case Carte.ROUGE:
	                        	bloc.setFill(Color.RED);
	                            break;
	                        case Carte.JAUNE:
	                        	bloc.setFill(Color.YELLOW);
	                            break;
	                        case Carte.BLEUE:
	                        	bloc.setFill(Color.BLUE);
	                            break;
	                        case Carte.VERT:
	                        	bloc.setFill(Color.GREEN);
	                            break;
                        }
                        children.add(bloc);
                        break;
                    }
                    case Carte.JOKER:
                    	Text joker = new Text("joker"); // pour changer la couleur
                    	int sizeTexte = (int) Math.rint(40);
                    	joker.setFont(new Font(sizeTexte));
                        switch (couleur) {
	                        case Carte.NOIR:
	                        	joker.setFill(Color.BLACK);
	                            break;
	                        case Carte.ROUGE:
	                        	joker.setFill(Color.RED);
	                            break;
	                        case Carte.JAUNE:
	                        	joker.setFill(Color.YELLOW);
	                            break;
	                        case Carte.BLEUE:
	                        	joker.setFill(Color.BLUE);
	                            break;
	                        case Carte.VERT:
	                        	joker.setFill(Color.GREEN);
	                            break;
                        }
                        children.add(joker);
                        break;
                    default:
                        break;
                }
            } else {
                Text textNumero = new Text(Integer.toString(numero));
                int sizeTexte = (int) Math.rint(100);
                textNumero.setFont(new Font(sizeTexte));
                
                switch (couleur) {
                    case Carte.NOIR:
                        textNumero.setFill(Color.BLACK);
                        break;
                    case Carte.ROUGE:
                        textNumero.setFill(Color.RED);
                        break;
                    case Carte.JAUNE:
                        textNumero.setFill(Color.YELLOW);
                        break;
                    case Carte.BLEUE:
                        textNumero.setFill(Color.BLUE);
                        break;
                    case Carte.VERT:
                        textNumero.setFill(Color.GREEN);
                        break;
                }

                children.add(textNumero);
            }

            StackPane carte = new StackPane();

            children.stream().forEach((node) -> {
                carte.getChildren().add(node);
            });

            return new Group(carte);
        
    }

    private static boolean isSpecial(int numero) {
        return numero == Carte.BLOC || numero == Carte.JOKER || numero == Carte.PREND_DEUX || numero == Carte.QUATRE || numero == Carte.RENVERSE;
    }
}
