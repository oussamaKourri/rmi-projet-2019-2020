package unoclient;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import classesrmi.ListJoueurs;
import interfacesrmi.IGestionnairePartie;
import interfacesrmi.IPartie;

public class EcranAccueil {

    private IGestionnairePartie gestionnaire;
    private Communicateur communicateur;

    public void initialiseEcran(IGestionnairePartie gestionnaire, Communicateur communicateur) {
        this.communicateur = communicateur;
        this.gestionnaire = gestionnaire;

        Button buttonEntrer = new Button("Entrer dans une partie �xistante");
        buttonEntrer.setOnAction((ActionEvent event) -> {
            new Thread(() -> entrerPartie()).start();
        });

        Button buttonCreePartie = new Button("Cr�e une partie");
        buttonCreePartie.setOnAction((ActionEvent event) -> {
            creePartie();
        });

        VBox vBox = new VBox(buttonEntrer, buttonCreePartie);
        vBox.setAlignment(Pos.CENTER);
        vBox.setPadding(new Insets(10));
        vBox.setSpacing(20);

        StackPane root = new StackPane(vBox);

        ClientRMI.setScene(root);
    }

    private void entrerPartie() {
        try {
            List<ListJoueurs> list = gestionnaire.listePartie();

            if (list.size() < 1) {
                ClientRMI.messageErreur("Non disponible");
            } else {
                new EcranListePatie().initialiseEcran(gestionnaire, list, communicateur);
            }
        } catch (RemoteException ex) {
            System.out.println(ex.toString());
        }

    }

    private void creePartie() {
        Dialog<ListJoueurs> dialog = new Dialog<>();
        dialog.setTitle("Creation d'une partie UNO");
        dialog.setResizable(true);

        Label label1 = new Label("nom: ");
        Label label2 = new Label("Nombre de joueures: ");
        TextField text1 = new TextField();
        TextField text2 = new TextField();

        GridPane grid = new GridPane();
        grid.add(label1, 1, 1);
        grid.add(text1, 2, 1);
        grid.add(label2, 1, 2);
        grid.add(text2, 2, 2);
        dialog.getDialogPane().setContent(grid);

        ButtonType buttonCreer = new ButtonType("Cr�er", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().add(buttonCreer);

        final Button okButton = (Button) dialog.getDialogPane().lookupButton(buttonCreer);
        okButton.setDisable(true);

        text1.textProperty().addListener((observable, oldValue, newValue) -> {
            okButton.setDisable(newValue.trim().length() < 1);
        });

        text2.textProperty().addListener((observable, oldValue, newValue) -> {
            boolean bool;

            try {
                Integer.parseInt(newValue);
                bool = false;
            } catch (NumberFormatException ex) {
                bool = true;
            }

            okButton.setDisable(bool);
        });

        dialog.setResultConverter((ButtonType b) -> {
            if (b == buttonCreer) {
                return new ListJoueurs(Integer.parseInt(text2.getText()), 0, -1, text1.getText());
            }

            return null;
        });

        Optional<ListJoueurs> result = dialog.showAndWait();

        if (result.isPresent()) {
            try {
                ListJoueurs listeJ = result.get();
                IPartie patie = gestionnaire.creePartie(listeJ.getNom(), listeJ.getNombreJoeuer(), communicateur.getId());

                new EcranAttenteJoueur().EcranDemarage(gestionnaire, communicateur, patie);

            } catch (RemoteException ex) {
                System.out.println(ex.toString());
            }
        }
    }
}
