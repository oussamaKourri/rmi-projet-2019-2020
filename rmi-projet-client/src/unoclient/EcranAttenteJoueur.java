package unoclient;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import interfacesrmi.IGestionnairePartie;
import interfacesrmi.IPartie;

public class EcranAttenteJoueur {
    
    public void EcranDemarage(IGestionnairePartie gestionnaire, Communicateur communicateur, IPartie partie) {
        Text text = new Text("Joueurs en attente");
        text.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        VBox vBox = new VBox(text);
        vBox.setSpacing(20);
        vBox.setAlignment(Pos.CENTER);
        BorderPane borderPane = new BorderPane(vBox);

        Button retour = new Button("RETOUR");
        retour.setOnAction(event -> {
            new EcranAccueil().initialiseEcran(gestionnaire, communicateur);
        });

        HBox hBox = new HBox(retour);
        hBox.setPadding(new Insets(15));

        borderPane.setBottom(hBox);

        ClientRMI.setScene(borderPane);
        
        new Thread(() -> JoueurEnAttente(communicateur, partie)).start();
    }
    
    private void JoueurEnAttente(Communicateur c, IPartie partie) {
        while (!c.tousJoueursConnectes()) {
            Thread.yield();
        }
        EcranPartie.getEcranPartie().initialiseEcran(c, partie);
    }
}
