package unoclient;

import interfacesrmi.IComunicateurJoueur;
import classesrmi.Carte;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

public class Communicateur extends UnicastRemoteObject implements IComunicateurJoueur, Serializable{
    
    private final int id;
    private boolean joueursConnectes = false;
    
    private List<Carte> listCartes;
    private int numeroJoueur;
    private Carte premiereCarte;
    
    public List<Carte> getListCartes() {
        return listCartes;
    }

    public int getNumeroJoueur() {
        return numeroJoueur;
    }
    
    
    
    public void retourAjoutCarte(List<Carte> carte) throws RemoteException {
        EcranPartie.getEcranPartie().ajouterCarte(carte);
    }
    
    
    public void setScore(boolean actuel, int score) throws RemoteException {
        EcranPartie.getEcranPartie().setNbreCartePrise(score);
        EcranPartie.getEcranPartie().setCartePrise(actuel);
    }
    
    
    public void joueurGagne(int joueur) throws RemoteException {
        unoclient.ClientRMI.messageInformation((numeroJoueur == joueur ? "vous" : "joueur " + joueur) + " a gagn� la partie!");
        EcranPartie.getEcranPartie().finPartie();
    }
    
    
    public void quitteEtatUno(int joueur) throws RemoteException {
        unoclient.ClientRMI.messageInformation((numeroJoueur == joueur ? "vous" : "joueur " + joueur) + " quitt� �tat uno");
    }
    
    
    public void declarerUno(int joueur) throws RemoteException {
        unoclient.ClientRMI.messageInformation((numeroJoueur == joueur ? "vous" : "joueur " + joueur) + " => Uno!");
    }
    
    
    public void setJoueurSuivant(int joueur) throws RemoteException {
        EcranPartie.getEcranPartie().setTourJoueur(joueur);
    }
    
    
    public void setCarteTable(Carte c) throws RemoteException {
        EcranPartie.getEcranPartie().setCarteTable(c);
    }
    
    
    public void setCartePremiere(Carte c) throws RemoteException {
        premiereCarte = c;
    }
    
    
    public void setCarteMain(List<Carte> CarteMain) throws RemoteException {
        this.listCartes = CarteMain;
    }
    
    
    public void setNumJoueur(int nbreJoueur) throws RemoteException {
        this.numeroJoueur = nbreJoueur;
    }
    
    
    public int getId() throws RemoteException {
        return id;
    }
    
    public void renitialiser() {
        joueursConnectes = false;
    }
    
    public boolean tousJoueursConnectes() {
        return joueursConnectes;
    }
    
    
    public void partieComplete() throws RemoteException {
        unoclient.ClientRMI.messageErreur("Partie compl�te");
    }
    
    public Carte getCarteTable() {
        return premiereCarte;
    }
    
    
    public void setJoueursConnectes(boolean joueursConnecte) throws RemoteException {
        joueursConnectes = joueursConnecte;
    }
    
    public Communicateur(int id) throws RemoteException{
        this.id = id;
    }
}
